<!DOCTYPE HTML>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<head>



    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Paweł Dobrowolski - Portfolio</title>

    <meta name="description" content="Stworzę dla Ciebie wyjątkową stronę www! Zatrudnij programistę webowego: PHP, JavaScript, HTML, CSS, MySQL, WordPress, Joomla, Drupal." />
    <meta name="keywords" content="zamów, stronę, tworzenie, www, programista, portfolio, php, javascript, html, css, WordPress, Joomla, Drupal" />

    <link href='http://fonts.googleapis.com/css?family=Lato|Josefin+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <script src="http://code.jquery.com/jquery-1.9.0rc1.js"></script>
    <style>
        <%@include file="../css/style.css"%>

    </style>
    <script  type="text/javascript">
        <%@include file="../resources/js/timer.js"%>
    </script>
    <script charset="UTF-8">

        function oferta() {

            document.getElementById("tile5").innerHTML="Zmajomość podstaw programowania w języku Java iJavaScript<br/>Dobra znajomość httml i ccs<br/>Umiejętność tworzenia baz SQL i noSQL<br/>Zaangażowanie<br/>Logiczne myślenie<br/>Kreatywność<br/>Szybkie uczenie<br/>Łatwe nawiązywanie kontaktów";

        }
        function kim() {
            document.getElementById("tile5").innerHTML="Choć nie mam doświadczenia w tej branży od rozpoczęcia kursu Java w Software Development Academy wiem, że programowanie jest tym czego szukałem od dawna, a projekty amatorskie które wykonuje pochłaniają mnie bez reszty. Poznałem takie technologie jak Java Java Script CSS Html Bazy Danych Git Scrum Springa.";

        }
        function kontakt() {
            document.getElementById("tile5").innerHTML="Kontakt ze mną<br /><br />Telefon 696 04 90 75<br /><br />pawwel3@gmail.com<br /><br />";
        }
        function gry() {
            var gry=' Gry<br /><br /> <a href="fortuna" class="tilelink">Fotyuna</a>';
            document.getElementById("tile5").innerHTML=gry;

        }
        function cv() {
            document.getElementById("tile5").innerHTML="";

        }
        function programy() {
            document.getElementById("tile5").innerHTML="w przygotowaniu";
        }

    </script>
</head>

<body onload="odliczanie(),kim();">


<div id="container">

    <div class="rectangle">
        <div id="logo" onclick="kim()"><a>Paweł Dobrowolski</a></div>
        <div id="zegar">12:00:00</div>
        <div style="clear: both;"></div>
    </div>

    <div class="square">
        <div class="tile1" onclick="kim()">
            <a><i class="icon-user"></i><br />Kim jestem?</a>
        </div>

        <div class="tile1" onclick="oferta()">
            <i class="icon-laptop"></i><br />Co oferuję?
        </div>

        <div style="clear:both;"></div>

        <div class="tile2" onclick="cv()">
            <a><i class="icon-graduation-cap"></i><br />Curriculum vitae</a>
        </div>
        <div class="tile3" onclick="kontakt()">
            <a><i class="icon-mail"></i><br />Kontakt ze mną</a>
        </div>
        <div style="clear:both;"></div>

        <div class="tile4">
            <i>Talk is cheap. Show me the code!</i><br />- Linus Torvalds, twórca Linuxa
        </div>
    </div>
    <div class="square">
        <div class="tile5" id="tile5">
        </div>
        <div class="yt" onclick="gry()">Gry

        </div>
        <div class="fb" onclick="programy()">Programy

        </div>
        <div class="gplus">
            <a href="http://plus.google.com" target="_blank" class="sociallink"><i class="icon-gplus"></i></a>
        </div>
        <div class="tw">
            <a href="http://twitter.com" target="_blank" class="sociallink"><i class="icon-twitter"></i></a>
        </div>
        <div style="clear:both;"></div>
    </div>
    <div style="clear: both;"></div>

    <div class="rectangle">2019 &copy; Paweł Dobrowolski - Portfolio.  <i class="icon-mail-alt"></i> pawwel3@gmail.com</div>

</div>

</body>
</html>